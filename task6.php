<?php

// Connect to the database using MySQLi
$host = 'localhost';
$username = 'root';
$password = '';
$database = 'p8_exercise_backend';


$conn = new mysqli($host, $username, $password, $database);


if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}


// Insert data into the employee table
$sql = "insert into employee (first_name, last_name, middle_name, birthday, address) values ('Jane', 'Doe', 'Marie', '1990-01-01', '456 Elm Street')";
if ($conn->query($sql) === TRUE) {
    echo "New record created successfully";
} else {
    echo "Error: " . $sql . "<br>" . $conn->error;
}

// retrieve the first name, last name, and birthday of all employees in the table
$sql = "select first_name, last_name, birthday from employee";
$result = $conn->query($sql);
if ($result->num_rows > 0) {
    while($row = $result->fetch_assoc()) {
        echo "First Name: " . $row["first_name"]. " - Last Name: " . $row["last_name"]. " - Birthday: " . $row["birthday"]. "<br>";
    }
} else {
    echo "0 results";
}

// retrieve the number of employees whose last name starts with the letter 'D'
$sql =  "select count(*) as count from employee where last_name like 'D%'";
$result = $conn->query($sql);
if ($result->num_rows > 0) {
    while($row = $result->fetch_assoc()) {
        echo "Count: " . $row["count"]. "<br>";
    }
} else {
    echo "0 results";
}

//  retrieve the first name, last name, and address of the employee with the highest ID number
$sql =  "select first_name, last_name, address from employee where id = (select max(id) from employee)";
$result = $conn->query($sql);
if ($result->num_rows > 0) {
    while($row = $result->fetch_assoc()) {
        echo "First Name: " . $row["first_name"]. " - Last Name: " . $row["last_name"]. " - Address: " . $row["address"]. "<br>";
    }
} else {
    echo "0 results";
}

// Update data in the employee table
$sql = "update employee set address = '123 Main Street' where first_name = 'John'";
if ($conn->query($sql) === TRUE) {
    echo "Record updated successfully";
} else {
    echo "Error updating record: " . $conn->error;
}

// Delete data from the employee table
$sql = "delete from employee where last_name like 'D%'";
if ($conn->query($sql) === TRUE) {
    echo "Record deleted successfully";
} else {
    echo "Error deleting record: " . $conn->error;
}
$conn->close();

?>
